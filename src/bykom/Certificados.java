package bykom;

import java.io.File;

public class Certificados {

	public static void main(String[] args) {

		iniciar(args, false);

	}

	public static void iniciar(String[] args, boolean shutdown) {
		try {

			// -----------------------------------------------------------------

			long minutos = FileWatchdog.DEFAULT_DELAY;

			String SEP = File.separator;

			// -----------------------------------------------------------------

			if (args.length == 0) {
				System.out.println("falta la ruta de tomcat del WCAdmin");
				return;
			}

			// -----------------------------------------------------------------

			String main = args[0];

			if (main.trim().isEmpty()) {
				System.out.println("ejemplo de la ruta : C:\tomcat6 ");
				return;
			}

			// -----------------------------------------------------------------

			if (args.length >= 2) {

				try {

					minutos = Integer.parseInt(args[1].trim());
					minutos = minutos * 60000;

				} catch (Exception e) {
					// TODO: handle exception
				}

			}

			// -----------------------------------------------------------------

			String certDirectory = main.trim() + SEP + "certificados" + SEP;

			String certificadosTemporales = certDirectory + "tmp-cacerts";

			// -----------------------------------------------------------------

			long minhr = minutos / (60 * 1000) % 60;

			System.out.println("Chequeo de certificados para instalar ... ");
			System.out.println("Chequeando cada " + minhr + " minutos");
			System.out.println("java : " + System.getProperty("java.home"));

			// -----------------------------------------------------------------

			CertificadosFileWatchdog dog = new CertificadosFileWatchdog(
					certificadosTemporales, certDirectory);
			dog.setDelay(minutos);
			
			dog.run();

			// -----------------------------------------------------------------

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
