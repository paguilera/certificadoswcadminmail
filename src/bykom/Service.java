package bykom;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import org.boris.winrun4j.AbstractService;
import org.boris.winrun4j.ServiceException;

public class Service extends AbstractService {

	private String directorio;
	private String java_home;
	private String serviceName;

	File file;
	long lastModif = 0;
	boolean warnedAlready = false;

	@Override
	public int serviceMain(String[] args) throws ServiceException {

		// -----------------------------------------------------------------

		String SEP = File.separator;

		// -----------------------------------------------------------------

		String main = "C:" + SEP + "tomcat6";

		// C:\Sun\SDK\jdk\jre
		java_home = "C:" + SEP + "sun" + SEP + "sdk" + SEP + "jdk" + SEP
				+ "jre";

		if (args.length > 0) {

			for (String arg : args) {
				System.out.println("argumento = " + arg);
			}

			main = args[0];
		}

		if (args.length >= 1) {
			java_home = args[1].trim();
		}
		
		if (args.length >= 2) {
			serviceName = args[2].trim();
		}

		// -----------------------------------------------------------------

		directorio = main.trim() + SEP + "certificados" + SEP;

		String certificadosTemporales = directorio + "tmp-cacerts";

		// -----------------------------------------------------------------

		System.out.println("Chequeo de certificados para instalar ... ");
		System.out.println("JAVA_HOME = " + System.getProperty("java.home"));
		System.out.println("directorio : " + directorio);
		System.out.println("temp keystore : " + certificadosTemporales);

		// -----------------------------------------------------------------

		file = new File(certificadosTemporales);

		while (!shutdown) {

			checkAndConfigure();

			try {

				Thread.sleep(5000);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		System.out.println("shutdown service : " + shutdown);

		return 0;

	}

	protected void checkAndConfigure() {

		boolean fileExists = false;
		try {
			fileExists = file.exists();
		} catch (SecurityException e) {
			System.out.println("El archivo no s epuede leer, file:["
					+ file.getName() + "].");
		}

		if (fileExists) {
			long l = file.lastModified(); // this can also throw a
											// SecurityException
			if (l > lastModif) { // however, if we reached this point this
				lastModif = l; // is very unlikely.
				doOnChange();
				warnedAlready = false;
			}
		} else {
			if (!warnedAlready) {
				System.out.println(
						"[" + file.getName() + "] el archivo no existe.");
				warnedAlready = true;
			}
		}
	}

	private void doOnChange() {

		System.out.println("Hay cambios en : " + file.getName());
		System.out.println("Obteniendo certificados");

		File folder = new File(directorio);

		File[] files = folder.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".cer");
			}
		});

		for (final File file : files) {

			System.out.println("");
			System.out.println("Instalando : " + file.getName());

			instalar(file.getName());

		}

		reinicioElServicio();

	}

	private void instalar(String name) {

		String SEP = File.separator;

		// String JAVA_HOME = System.getProperty("java.home");

		// C:\Sun\SDK\jdk\jre\lib\security
		String keystore = java_home + SEP + "lib" + SEP + "security" + SEP
				+ "cacerts";

		String keystorepass = "changeit";

		String alias = name.replace(".cer", "");

		try {

			// elimino el alias por si ya esta agrgado -------------------------

			String delete = "keytool";
			delete += " -delete";
			delete += " -alias \"" + alias + "\"";
			delete += " -keystore \"" + keystore + "\"";
			delete += " -storepass " + keystorepass;
			delete += " -noprompt";

			System.out.println(delete);

			execute(delete);

		} catch (java.lang.Exception e) {
			// en caso que no existe el alias no importa si tira error
		}

		try {

			// agrego el alias -------------------------------------------------

			String add = "keytool";
			add += " -import";
			add += " -alias \"" + alias + "\"";
			add += " -keystore \"" + keystore + "\"";
			add += " -storepass " + keystorepass;
			add += " -file \"" + directorio + name + "\"";
			add += " -noprompt";

			System.out.println(add);

			execute(add);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void reinicioElServicio() {

		System.out.println("");
		System.out.println("Reinicio el servicio " + serviceName);
		System.out.println("");

		try {

			String service = serviceName; //"BykomWCAdmin3";

			// chequeo si el cervicio está corriendo ---------------------------

			if (serviceIsRunning(service)) {

				// para el servicio
				// ------------------------------------------------
				String serviceStop = "sc stop " + service;
				System.out.println(serviceStop);
				execute(serviceStop);

				// espera 1 minuto y medio 
				// ----------------------------------------------
				Thread.sleep(90*1000);

				// prende el servicio
				// ----------------------------------------------
				String serviceStart = "sc start " + service;
				System.out.println(serviceStart);
				execute(serviceStart);

				System.out.println("");

			} else {

				System.out.println(
						"El servicio " + service + " está parado o no existe.");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private boolean serviceIsRunning(String service) {

		String serviceRunning = "sc query " + service;

		System.out.println(serviceRunning); // RUNNING
		System.out.println("");
		try {

			Process p = Runtime.getRuntime().exec(serviceRunning);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(p.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null) {
				System.out.println(line);
				if (line.toLowerCase().contains("RUNNING".toLowerCase())) {
					return true;
				}
			}

		} catch (Exception e) {
			return false;
		}

		return false;
	}

	private void execute(String command) throws IOException {
		Process p = Runtime.getRuntime().exec(command);
		BufferedReader in = new BufferedReader(
				new InputStreamReader(p.getInputStream()));
		String line = null;
		while ((line = in.readLine()) != null) {
			// if (!line.contains("no existe") && !line.contains("not exist"))
			// {}
			System.out.println(line);

		}
	}

}
