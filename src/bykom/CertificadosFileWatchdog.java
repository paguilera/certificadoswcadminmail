package bykom;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CertificadosFileWatchdog extends FileWatchdog {

	private String log;
	private String directorio;

	private FileOutputStream logOutput;

	public CertificadosFileWatchdog(String filename, String directorio) {
		super(filename);

		this.directorio = directorio;

		// ---------------------------------------------------------------------

		stdoutLog();

		// ---------------------------------------------------------------------

	}

	public void doOnChange() {

		// ---------------------------------------------------------------------

		limpioElArchivoDeLogs();

		// ---------------------------------------------------------------------

		System.out.println("Hay cambios en : " + this.filename);
		System.out.println("Obteniendo certificados");

		File folder = new File(directorio);

		File[] files = folder.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".cer");
			}
		});

		for (final File file : files) {

			System.out.println("");
			System.out.println("Instalando : " + file.getName());

			instalar(file.getName());

		}

		// ---------------------------------------------------------------------

		reinicioElServicio();

		// ---------------------------------------------------------------------

	}

	private void limpioElArchivoDeLogs() {

		try {

			File file = new File(log);
			PrintWriter writer = new PrintWriter(file);
			writer.write("");
			writer.close();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void reinicioElServicio() {

		System.out.println("");
		System.out.println("Reinicio el servicio WCAdmin ");
		System.out.println("");

		try {

			String service = "BykomWCAdmin3";

			// chequeo si el cervicio está corriendo --------------------------- 

			if (serviceIsRunning(service)) {

				// para el servicio
				// ------------------------------------------------
				String serviceStop = "sc stop " + service;
				System.out.println(serviceStop);
				execute(serviceStop);

				// espera 10 segundos
				// ----------------------------------------------
				Thread.sleep(10000);

				// prende el servicio
				// ----------------------------------------------
				String serviceStart = "sc start " + service;
				System.out.println(serviceStart);
				execute(serviceStart);

				System.out.println("");

			} else {

				System.out.println("El servicio " + service + " está parado o no existe.");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private boolean serviceIsRunning(String service) {

		String serviceRunning = "sc query " + service;

		System.out.println(serviceRunning); // RUNNING
		System.out.println("");
		try {

			Process p = Runtime.getRuntime().exec(serviceRunning);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(p.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null) { 
				System.out.println(line);
				if(line.toLowerCase().contains("RUNNING".toLowerCase())){
					return true;
				}				
			}

		} catch (Exception e) {
			return false;
		}

		return false;
	}

	private void instalar(String name) {

		String SEP = File.separator;

		// %JAVA_HOME% = C:\Sun\SDK\jdk\jre\lib\security

		// "C:\sun\sdk\jdk"

		String JAVA_HOME = System.getProperty("java.home");

		// JAVA_HOME = "%JAVA_HOME%" + SEP + "jre";

		// JAVA_HOME =

		String keystore = JAVA_HOME + SEP + "lib" + SEP + "security" + SEP
				+ "cacerts";

		String keystorepass = "changeit";

		String alias = name.replace(".cer", "");

		try {

			// elimino el alias por si ya esta agrgado -------------------------

			String delete = "keytool";
			delete += " -delete";
			delete += " -alias \"" + alias + "\"";
			delete += " -keystore \"" + keystore + "\"";
			delete += " -storepass " + keystorepass;
			delete += " -noprompt";

			System.out.println(delete);

			execute(delete);

		} catch (java.lang.Exception e) {
			// en caso que no existe el alias no importa si tira error
		}

		try {

			// agrego el alias -------------------------------------------------

			String add = "keytool";
			add += " -import";
			add += " -alias \"" + alias + "\"";
			add += " -keystore \"" + keystore + "\"";
			add += " -storepass " + keystorepass;
			add += " -file \"" + directorio + name + "\"";
			add += " -noprompt";

			System.out.println(add);

			execute(add);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void execute(String command) throws IOException {
		Process p = Runtime.getRuntime().exec(command);
		BufferedReader in = new BufferedReader(
				new InputStreamReader(p.getInputStream()));
		String line = null;
		while ((line = in.readLine()) != null) {
			// if (!line.contains("no existe") && !line.contains("not exist"))
			// {}
			System.out.println(line);

		}
	}

	private void stdoutLog() {
		try {

			log = directorio + "stdout.log";

			logOutput = new FileOutputStream(log);

			BufferedOutputStream buffer = new BufferedOutputStream(logOutput);

			final PrintStream original = System.out;

			PrintStream stream = new PrintStream(buffer, true) {
				@Override
				public void println(String s) {
					String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.format(new Date());
					super.println(String.format("%s %s", date, s));
					original.println(s);
				}
			};

			System.setOut(stream);
			System.setErr(stream);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}